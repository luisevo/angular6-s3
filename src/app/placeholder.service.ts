import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PlaceholderService {

  constructor(private http: HttpClient) { }

  getPosts() {
    return this.http.get('https://jsonplaceholder.typicode.com/posts');
  }

  getComments(postId: string) {
    return this.http.get(`https://jsonplaceholder.typicode.com/comments?postId=${postId}`);
    // return this.http.get('https://jsonplaceholder.typicode.com/comments?postId='+postId);
  }

  addComment(params: {postId, name, email, body}) {
    return this.http.post('https://jsonplaceholder.typicode.com/comments', params);
  }
}
