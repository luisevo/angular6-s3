import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Angular app';

  today = new Date();

  money = 300.00;

  a = 0.25;

  post = {
    title: 'Mi Primera publicacion',
    date: new Date()
  };

  myPromise = new Promise((resolve, reject) => {
    // reject('Esto es un error');
    resolve('Esta es la respuesta correcta');
  });

  constructor() {
  }

}
