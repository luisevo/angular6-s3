import { Component, OnInit } from '@angular/core';
import {PlaceholderService} from '../../placeholder.service';

class Post {
  userId: number;
  id: number;
  title: string;
  body: string;
}

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss']
})
export class PostsComponent implements OnInit {
  records: Post[] = [];

  constructor(private placeholder: PlaceholderService) { }

  ngOnInit() {
    this.placeholder.getPosts()
      .subscribe(
        (result: Post[]) => {
          console.log('Este es la respuesta correcta: ', result);
          this.records = result;
        },
        error => console.log('Este es un error: ', error),
        () => console.log('Esta peticion acabo')
      );
  }

}
