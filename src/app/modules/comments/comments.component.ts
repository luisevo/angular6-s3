import { Component, OnInit } from '@angular/core';
import {PlaceholderService} from '../../placeholder.service';
import {Observable} from 'rxjs';
import {ActivatedRoute} from '@angular/router';

class Comment {
  postId: number;
  id: number;
  name: string;
  email: string;
  body: string;
}

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.scss']
})
export class CommentsComponent implements OnInit {
  records: Comment[] = [];

  postId;
  name: string;
  email: string;
  body: string;

  constructor(
    private placeholder: PlaceholderService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {

    this.postId = this.route.snapshot.paramMap.get('postId');

    this.placeholder.getComments(this.postId)
      .subscribe(
        (result: Comment[]) => this.records = result,
        (error) => console.log('error')
      );
  }

  add() {
    const params = {
      postId: this.postId,
      name: this.name,
      email: this.email,
      body: this.body
    };
    this.placeholder.addComment(params)
      .subscribe(
        (result: Comment) => {
          this.records.unshift(result);
        },
        error => console.log(error),
      );
  }

}
